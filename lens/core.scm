;; lens/core.scm --- core implementation    -*- coding: utf-8 -*-
;;
;; Copyright (C) 2019 Alex Sassmannshausen <alex@pompo.co>
;;
;; Author: Alex Sassmannshausen <alex@pompo.co>
;;
;; This file is part of guile-lens.
;;
;; guile-lens is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by the Free
;; Software Foundation; either version 3 of the License, or (at your option)
;; any later version.
;;
;; guile-lens is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
;; for more details.
;;
;; You should have received a copy of the GNU General Public License along
;; with guile-lens; if not, contact:
;;
;; Free Software Foundation           Voice:  +1-617-542-5942
;; 59 Temple Place - Suite 330        Fax:    +1-617-542-2652
;; Boston, MA  02111-1307,  USA       gnu@gnu.org

;;; Commentary:
;;
;;; Code:

(define-module (lens core)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-26)
  #:export (lens focus over put id-setter))

(define (lens getter setter)
  "Return a composable lens, which can be used to focus, apply functions over,
or update a value in an arbitrary data structure.

GETTER is a procedure of one argument, the data structure, that can be used to
access the focused item in the data structure.

SETTER is a procedure of two arguments, the data structure & a procedure to be
applied to the data structure to transform the focused value."
  (lambda (previous)
    (match-lambda*
      ((s)
       (getter (previous s)))
      ((s f) (previous s (cut setter <> f))))))

(define (id-setter s f)
  "The identity setter. IDENTITY is the identity getter."
  (f s))

(define (focus lens s)
  "Focus on a value in the data structure S by using the lens LENS."
  (let ((getter (lens identity)))
    (getter s)))

(define (over lens f s)
  "Apply the function F over the value focused on by the lens LENS in the data
structure S."
  (let ((setter (lens id-setter)))
    (setter s f)))

(define (put lens v s)
  "Replace the value focused on by the lens LENS with the new value V in data
structure S."
  (over lens (const v) s))
