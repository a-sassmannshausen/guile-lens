(hall-description
  (name "lens")
  (prefix "guile")
  (version "0.1")
  (author "Alex Sassmannshausen")
  (copyright '(2018))
  (synopsis
    "Composable lenses for data structures in Guile")
  (description
    "Guile-Lens is a library implementing lenses in Guile.  The library is currently a re-implementation of the lentes library for Clojure. Lenses provide composable procedures, which can be used to focus, apply functions over, or update a value in arbitrary data structures.")
  (home-page
    "https://gitlab.com/a-sassmannshausen/")
  (license gpl3+)
  (dependencies `())
  (files (libraries
           ((scheme-file "lens")
            (directory
              "lens"
              ((scheme-file "lenses") (scheme-file "core")))))
         (tests ((directory "tests" ((scheme-file "lens")))))
         (programs ((directory "scripts" ())))
         (documentation
           ((text-file "README")
            (text-file "HACKING")
            (text-file "COPYING")
            (directory "doc" ((texi-file "lens")))))
         (infrastructure
           ((scheme-file "guix") (scheme-file "hall")))))
