(use-modules
  (guix packages)
  (guix licenses)
  (guix download)
  (guix build-system gnu)
  (gnu packages)
  (gnu packages autotools)
  (gnu packages guile)
  (gnu packages pkg-config)
  (gnu packages texinfo))

(package
  (name "guile-lens")
  (version "0.1")
  (source "./guile-lens-0.1.tar.gz")
  (build-system gnu-build-system)
  (arguments `())
  (native-inputs
    `(("autoconf" ,autoconf)
      ("automake" ,automake)
      ("pkg-config" ,pkg-config)
      ("texinfo" ,texinfo)))
  (inputs `(("guile" ,guile-2.2)))
  (propagated-inputs `())
  (synopsis
    "Composable lenses for data structures in Guile")
  (description
    "Guile-Lens is a library implementing lenses in Guile.  The library is currently a re-implementation of the lentes library for Clojure. Lenses provide composable procedures, which can be used to focus, apply functions over, or update a value in arbitrary data structures.")
  (home-page
    "https://gitlab.com/a-sassmannshausen/")
  (license gpl3+))

